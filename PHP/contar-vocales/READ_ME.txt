
Contar Vocales
(Count Vowels)

READ ME


ESP:

Coge una frase del usuario y cuenta el numero de vocales en la frase.
Devuelve el numero de vocales en forma numero romano.

Por ejemplo:

Entrada: “María ha ganado el primer premio de la lotería nacional”
Salida:  “XXIII vocales”


-------------------------------------------------------

ENG:

Get a phrase from the user and count the number of vowels in the phrase.
Return the number of vowels in Roman Numerals.

For example:

Input: “María ha ganado el primer premio de la lotería nacional”
Output:  “XXIII vocales”

