<?php

/*
	countVowels.php
	ENG: Takes userInput and returns number of vowels in input in Roman Numerals.
	ESP: Coge string de usuario y vuelve el numero de vocales del string como numeros romanos.x
	@author Josh McCarthy
*/

/*
	MAIN method.
	@param $userInput, string
	@return string
*/
function countVowelsMain($userInput){
	$cleanInput = prepInput($userInput);
	$numVowels = countVowels($cleanInput);
	return convertToRomans($numVowels);
}

/*
	Prepares $input - remove HTML chars and change $input to lowercase.
	@param $input, string
	@return clean string
*/
function prepInput($input){
	return strtolower(htmlspecialchars($input));
}

/*
	Count vowels in string.
	@param $str, string
	@return number of vowels (Spanish) in $str
*/
function countVowels($str){
	return preg_match_all('/[aeiouéíóá]/ui',$str,$matches);
}

/*
	Converts $num to Roman Numerals.
	@param $num, integer
	@return Roman Numeral string
*/
function convertToRomans($num){
	$rNumerals = array(
		'M'  => 1000,
	    'CM' => 900,
	    'D'  => 500,
	    'CD' => 400,
	    'C'  => 100,
	    'XC' => 90,
	    'L'  => 50,
	    'XL' => 40,
	    'X'  => 10,
	    'IX' => 9,
	    'V'  => 5,
	    'IV' => 4,
	    'I'  => 1);

	$result = '';

	foreach ($rNumerals as $letter => $value) {
		$matches = floor($num/$value);
		$num = $num - ($value * $matches);
		$result .= str_repeat($letter, $matches);
	}

	return $result;
}

?>