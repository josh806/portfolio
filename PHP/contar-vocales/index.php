<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="icon" href="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="libs/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="libs/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		
		<title>Contar Vocales</title>
	</head>
	<body>

		<div class="container">
			<h1>Contar vocales con numeros romanos</h1>

			<form action="" method="POST">
				Por favor, ponga un frase:</br>
				<input type="text" name="phrase"></br>
				<input type="submit" value="Contar Vocales" name="countVowels" style="margin-top: 10px;">
			</form>

			<?php

				//include 'countVocals.php';
				if(!@include("countVowels.php")){
					echo "No puede encontrar 'countVowels.php'";
					throw new Exception("No puede encontrar 'countVowels.php'");
					$answer = "No puede encontrar 'countVowels.php'";
				}

				$phrase = "";
				$answer = "";

				// if "Contar Vocales" button pressed
				if (isset($_POST["countVowels"])){
					$phrase = $_POST["phrase"];
					if(strlen($phrase) == 0){
						$answer = "No ha puesto una frase";
					}else{
						$answer = countVowelsMain($_POST["phrase"]);
					}
				}
			?>

			<p>
				<?php echo $phrase . "<br>" . $answer; ?>
			</p>

		</div>

	</body>
</html>