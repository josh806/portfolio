/*
	edit-training.js
	edit-training.html functionality.
*/


var editTraining = {
    languages: [{id:"es", name:"Spanish"}, {id:"en", name:"English"}],
    tObj: {},
    saveRow: "",//used in edit
    trainingsObj: {},
    tUrl: null,


    initialize: function(){
        var self = this,
            printStatus = "",
            i;
                
        self.tUrl = getParameterByName('t');
            
        $("h1").empty();

        for(i=0; i<self.languages.length; i++){
            self[self.languages[i].id + "Trainings"] = [];//create array for each lang
            self.printLangSection(self.languages[i].id, self.languages[i].name);//print language sections
        }

        // Check that training exists or create new training
        if(!self.trainingsObj[self.tUrl]){
            //Training doesn't exist

            if(self.tUrl !== null){
                $("#bootstrap-override").prepend('<p class="alert alert-danger"><strong>ERROR!</strong> The training you were looking for doesn\'t exist. Create a new training below or return to the <a href="trainings.html">Trainings page</a>.</p>');
            }

            $('h1').append("Create Training");

            printStatus = "create";

        }else{
            //Training exists

            tObj = self.trainingsObj[self.tUrl];

            $("h1").append(tObj.en.title);
            $("#order").val(tObj.order);

            // Print fields for each language and create lang arrays.
            for(i=0; i<self.languages.length; i++){
                var lang = self.languages[i],
                    tArray = tObj[lang.id].trainings;

                self.printFields(lang.id, tObj[lang.id]);
                
                for(var j=0; j<tArray.length; j++){
                    self[lang.id + "Trainings"].push(tArray[j]);
                }
            }
        }

        for(i=0; i<self.languages.length; i++){
            self.printTable(self.languages[i].id, printStatus);
        }

    },

    /* Order training arrays by .date */
    orderArrays: function(){
        var self = this;

        for(var i=0; i<self.languages.length; i++){
            var arrayName = self.languages[i].id + "Trainings";

            self[arrayName].sort(function(a,b){

                // date format
                var aDate = moment(a.date, "DD/MM/YYYY HH:mm"),
                    bDate = moment(b.date, "DD/MM/YYYY HH:mm");

                return aDate - bDate;
            });
        }
    },

    /*
        Prints fields and table of training dates for language.
        @param langId, e.g. "es"
        @param langName, e.g. "Spanish"
    */
    printLangSection: function(langId, langName){
        var lgTemplate = '<div id="' + langId + '" class="section row">' + 
                            '<h2>' + langName + '</h2>' + 
                            '<div class="row">' + 
                                '<div class="col-md-6">' +
                                    '<div class="form-group">' +
                                        '<label for="title-' + langId + '">Title:</label>' +
                                        '<input type="text" id="title-' + langId + '" class="form-control">' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label for="formid-' + langId + '">Training ID:</label>' +
                                    '<input type="text" id="formid-' + langId + '" class="form-control">' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group col-md-6">' +
                                '<label for="descShort-' + langId + '">Short Description:</label>' +
                                '<textarea class="form-control" rows="5" id="descShort-' + langId + '"></textarea>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label for="descLong-' + langId + '">Long Description:</label>' +
                            '<textarea class="form-control" rows="5" id="descLong-' + langId + '"></textarea>' +
                        '</div>' +
                        
                        '<div class="table-div">' +
                                    
                          '<table id="table-' + langId + '" class="table table-hover">' +
                            '<thead>' +
                              '<tr>' +
                                '<th>Date</th>' +
                                '<th>Webinar Key</th>' +
                                '<th>Trainer</th>' +
                              '</tr>' +
                            '</thead>' +
                            '<tbody></tbody>' +
                         '</table>' +
                       '</div>' +
                      '</div>';
        $("#lng-sections").append(lgTemplate);
    },

    /*
        Fill in the fields associated to languages.
        @param id, css id
        @param trainingLang, object e.g. en, es
    */
    printFields: function(id, trainingLang){
        $("#title-" + id).val(trainingLang.title);
        $("#formid-" + id).val(trainingLang.formId);
        $("#descShort-" + id).val(trainingLang.descShort);
        $("#descLong-" + id).val(trainingLang.descLong);
    },

    /*
        Print trainings date table.
        @param tableId, table id "table_%%"
        @param printStatus, used in switch statement
        @param rowId, optional param, id of row to edit
    */
    printTable: function(tableId, printStatus, rowId){

        // Check that no extra training date form rows appear in tables
        if($('[id="form-row"]').length > 1){
            window.alert("Error on page.\n\nPress 'ok' to refresh the page");
            window.setTimeout('location.reload()', 1000); //Reloads page after one second
        }else{

            rowId = (rowId === undefined) ? "form-row" : rowId;//if regular row or edit form row

            var self = this,
                tableBody = $("#table-" + tableId + ">tbody"),
                arrayName = tableId + "Trainings",
                removeButtons = function(){
                        $(".add-training").remove();
                        $("#save-form").attr("disabled", true);
                        $(".edit-training").hide();
                        $(".delete-training").hide();
                    },
                addButtons = function(){
                        //Add training button to all language tables
                        for(var i=0; i<self.languages.length; i++){
                            addTrainingBtn(self.languages[i].id);
                        }
                        $("#save-form").attr("disabled", false);
                        $(".edit-training").show();
                        $(".delete-training").show();
                    },
                addTrainingBtn = function(tableIdLocal){
                        $("#table-" + tableIdLocal + ">tbody").append('<tr class="add-training"><td colspan="100"><button class="btn btn-default" type="button" onclick="editTraining.printTable(\'' + tableIdLocal + '\', \'add\')">Add training date</button></td></tr>');
                    },
                //empty form table row
                formRow = '<tr id="form-row"><td class="form-group td-date">'+
                            '<div class="input-group date datetimepicker" id="datetimepicker">' + 
                                '<input type="text" class="form-control">' + 
                                '<span class="input-group-addon">' +
                                    '<span class="glyphicon glyphicon-calendar"></span>' +
                                '</span>' +
                            '</div>' +
                            '<script type="text/javascript">$(function () {$("#datetimepicker").datetimepicker({format: "DD/MM/YYYY HH:mm"});});</script>' +
                        '</td>' + 
                        '<td class="form-group td-wk"><div>' +
                            '<input type="text" id="wk">' +
                        '</div></td>' + 
                        '<td class="form-group td-trainer"><div>' +
                            '<input type="text" id="trainer">' +
                        '</div></td>' +
                        '<td class="button-col"><button id="save-date" class="btn btn-default" type="button" onclick="editTraining.printTable(\'' + tableId + '\', \'%save-type%\')">Save</button>' + 
                        '<button id="cancel-save" class="btn btn-default" type="button" onclick="editTraining.printTable(\'' + tableId + '\', \'cancel\', \'' + rowId + '\')">Cancel</button></td></tr>';

            switch(printStatus){
                case "create":
                    self.addNoTrainingsMsg(tableId, "append");
                    addTrainingBtn(tableId);
                    $("#save-form").attr("disabled", false);
                    break;
                case "add":
                    $("#table-" + tableId + " .no-trainings").remove();
                    removeButtons();

                    formRow = formRow.replace("%save-type%", "save-new");

                    //append empty form as table row
                    tableBody.append(formRow);
                    break;
                case "edit":
                    $("#table-" + tableId + " .no-trainings").remove();
                    removeButtons();

                    formRow = formRow.replace("%save-type%", "save-edit");

                    // tmp save data from row
                    var tmpDate = $("#" + rowId + " .date-cell").text(),
                        tmpWk = $("#" + rowId + " .wk-cell").text(),
                        tmpTrainer = $("#" + rowId + " .trainer-cell").text();

                    // save row in case of cancel
                    self.saveRow = $("#" + rowId);

                    $("#" + rowId).replaceWith(formRow);

                    $("#wk").attr("disabled", "disabled");

                    // print form fields with existing values
                    $("#datetimepicker>input").val(tmpDate);
                    $("#wk").val(tmpWk);
                    $("#trainer").val(tmpTrainer);

                    break;
                case "cancel":
                    if(rowId === "form-row"){
                        //new: remove form from table
                        $("#form-row").remove();
                    }else{
                        //edit: change form back to regular row
                        $("#form-row").replaceWith(self.saveRow);
                    }

                    if(self[tableId + "Trainings"].length === 0){
                        self.addNoTrainingsMsg(tableId, "append");
                    }

                    addButtons();
                    break;
                case "save-new":
                case "save-edit":
                    $("#wk-exists").remove();
                    
                    var tDate = "#datetimepicker>input",
                        tWk = "#wk",
                        tTrainer = "#trainer",
                        errFound = false;

                    if(isEmptyField(tDate)) errFound = true;
                    if(isEmptyField(tWk)) errFound = true;
                    if(isEmptyField(tTrainer)) errFound = true;
                    if(errFound) break;

                    if(printStatus === "save-new"){
                        // make sure wk is unique
                        if(self.wkExists(arrayName)){
                            break;
                        }
                        
                        var newTraining = {
                            active: true,
                            date: $(tDate).val(),
                            goToWebinarWebinarKey: $(tWk).val(),
                            trainer: $(tTrainer).val()
                        };

                        //no errors, so add new training date to array
                        self[arrayName].push(newTraining);
                    }else{
                        //save-edit

                        var array = self[arrayName];

                        //find and update entry in array
                        for(var i=0; i<array.length; i++){
                            if(array[i].goToWebinarWebinarKey === $(tWk).val()){
                                array[i].date = $(tDate).val();
                                array[i].trainer = $(tTrainer).val();
                            }
                        }
                    }

                    self.orderArrays();
                    tableBody.empty();
                    for(var i=0; i<self[arrayName].length; i++){
                        tableBody.append(self.buildHtml(tableId, self[arrayName][i]));
                    }
                    addButtons();
                    self.adviseToSave(tableId);
                    break;
                default:
                    //if no trainings left, print msg
                    if(!self[tableId + "Trainings"].length){
                        self.addNoTrainingsMsg(tableId, "prepend");
                    }else{
                        self.orderArrays();
                        tableBody.empty();
                        for(var i=0; i<self[arrayName].length; i++){
                            tableBody.append(self.buildHtml(tableId, self[arrayName][i]));
                        }
                    }
                    addTrainingBtn(tableId);
                    $("#save-form").attr("disabled", false);
            }

            return false;
        }

    },

    /*
        Forms html to use in printData.
        @param tableId, id of table
        @param curr, current row object
        @return table row to print
    */
    buildHtml: function(tableId, curr){
        var HTMLtraining =  '<tr id="%wk%"><td class="date-cell">%date%</td><td class="wk-cell">%wk%</td><td class="trainer-cell">%trainer%</td>' + 
                            '<td class="button-col"><a class="edit-training btn-lg" href="javascript:editTraining.printTable(\'' + tableId + '\', \'edit\', \'%wk%\');"><i class="fa fa-pencil" aria-hidden="true"></i></a>' + 
                            '<a class="delete-training btn-lg" href="javascript:confirmDelete(\'%wk%\', \'' + tableId + '\');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>',
            tmpStr = HTMLtraining.replace('%date%', curr.date);

        tmpStr = tmpStr.replace(/%wk%/g, curr.goToWebinarWebinarKey);
        tmpStr = tmpStr.replace('%trainer%', curr.trainer);

        return tmpStr;
    },

    /*
        Print message if no trainings exist.
        @param langId
        @param where, append or prepend
    */
    addNoTrainingsMsg: function(langId, where){
        var table = $("#table-" + langId + ">tbody");
            msg = '<tr class="no-trainings"><td colspan="100"><p>You don\'t have any trainings planned</p></td></tr>';
            
        if(where === "prepend"){
            table.prepend(msg);
        }else table.append(msg);
    },

    /*
        Print message advising user to save changes.
        @param tableIdLocal
    */
    adviseToSave: function(tableIdLocal){
        if(!$("#" + tableIdLocal + " .alert-info").length){
            $("#" + tableIdLocal).append('<p class="alert alert-info">Save the form at the bottom of the page for changes to take effect</p>');
        }
    },

    /*
        If wk entered by user already exists, print error and return true,
        otherwise return false.
        @param arrayName, array to check for wk
        @return true or false
    */
    wkExists: function(arrayName){
        var self = this,
            errClass = "invalid",
            error = "Webinar Key already exists",
            wk = $("#wk").val();

        $("#wk").removeClass(errClass);

        for(var i=0; i<self[arrayName].length; i++){
            if(self[arrayName][i].goToWebinarWebinarKey === wk){
                $("#wk").addClass(errClass);
                $("#form-row .td-wk").append('<p id="wk-exists" class="invalid-text">' + error + '</p>');
                return true;
            }
        }
        return false;
    },

    /* 
        Delete training date from trainings.json.
        Update trainings.json.
        @param wk, webinar key of training date to delete
    */
    deleteTraining: function(wk, tableId){
        var self = this,
            arrayName = tableId + "Trainings";
        
        for(var i=0; i<self[arrayName].length; i++){
            for(key in self[arrayName][i]){
                if(self[arrayName][i].goToWebinarWebinarKey === wk){
                    self[arrayName].splice(i, 1);
                    break;
                }
            }
        }

        self.printTable(tableId);

        //alert user to save form
        self.adviseToSave(tableId);

        //if no trainings left, print msg
        if(!self[tableId + "Trainings"].length){
            self.addNoTrainingsMsg(tableId, "prepend");
        }
    },

    saveData: function(){
        var self = this;

        $(".invalid-text").remove();

        //check fields
        if(self.invalidFields()){
            console.log("Errors on page");
        }else{

            var ref = self.tUrl,
                newObj = {};

            if(!self.trainingsObj[self.tUrl]){//if training doesn't exist
                ref = self.createTrainingRef("#title-en");//create ref using english title
            }

            /*-- Update/create training --*/
            newObj.order = $("#order").val();
            
            for(i=0; i<self.languages.length; i++){
                var lang = self.languages[i];                

                newObj[lang.id] = {};
                newObj[lang.id].title = $("#title-" + lang.id).val();
                newObj[lang.id].formId = $("#formid-" + lang.id).val();
                newObj[lang.id].descShort = $("#descShort-" + lang.id).val();
                newObj[lang.id].descLong = $("#descLong-" + lang.id).val();

                newObj[lang.id].trainings = self[lang.id + "Trainings"];//trainings array

            }
            /*-- END Update/create training --*/

            self.trainingsObj[ref] = newObj;

            $.when(writeJSON(self.trainingsObj))
                .then(window.location.href = "trainings.html?s=success")
        }
    },

    /*
        Validates fields on edit-trainings.html (excluding table fields).
        @return true if valid, otherwise false
    */
    invalidFields: function(){
        $("#order-err").remove();

        var errFound = false,
            self = this;

        //Order
        if(isEmptyField("#order")){
            errFound = true;
        }else if($.isNumeric($("#order").val()) == false){
            $("#order").parent().append('<p id="order-err" class="invalid-text">Order must be a number</p>');
        }

        //lang fields (title, formid, descShort, descLong)
        for(var i=0; i<self.languages.length; i++){
            var lang = self.languages[i].id;

            if(isEmptyField("#title-" + lang)) errFound = true;
            if(!containsLetters("#title-" + lang)) errFound = true;

            if(isEmptyField("#formid-" + lang)) errFound = true;
            if(isEmptyField("#descShort-" + lang)) errFound = true;
            if(isEmptyField("#descLong-" + lang)) errFound = true;
        }

        return errFound;
    },

    /*
        Creates a training array reference from form field value.
        @param id of form field
        @return string reference
    */
    createTrainingRef: function(id){
        console.log("createTrainingRef");
        var self = this,
            ref = $(id).val(),
            uniqueKey = false,
            i = 1;

        if(!ref) console.log("$(" + id + ").val() not defined");//safeguard

        ref = ref.replace(/[^a-z]/gi, '');
        ref = ref.toLowerCase();

        //check new ref doesn't exist
        //if doesn't exist create new ref
        while(!uniqueKey){
            for(key in self.trainingsObj){
                if(key.toLowerCase() === ref){
                    if(ref.indexOf('_') !== -1){
                        ref = ref.substring(0,key.indexOf('_'));
                    }
                    ref = ref + "_" + i;
                    i++;
                    uniqueKey = false;
                }else uniqueKey = true;
            }
        }

        return ref;
    }
};


$(document).ready(function() {
    $.when(getJSON())
        .then(function(){
            editTraining.trainingsObj = arrayToObject(trainingsArray);
            editTraining.initialize();

            $("#save-form").click(function(){
            	window.onbeforeunload = null;
            });

            window.onbeforeunload = function(e) {
               return "Save the training before leaving page";
            };
        })
});