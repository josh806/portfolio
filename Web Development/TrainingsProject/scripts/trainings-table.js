/*
    trainings-table.js
    trainings.html funcionality.
*/
    

var trainingsTable = {

    initialize: function(){
        this.orderArray();
        this.printTrainings();
    },

    orderArray: function(){
        trainingsArray.sort(function(a, b){
            var orderA,
                orderB;

            for(keyA in a){
                for(keyB in b){
                    orderA = (a[keyA].order) ? a[keyA].order : -1;//to sync trainings that are missing order variable
                    orderB = (b[keyB].order) ? b[keyB].order : -1;
                    
                    //other way around to ensure trainings without order variable are last
                    return orderB - orderA;
                }
            }
        });
    },

    /* Print trainings table on trainings.html */
    printTrainings: function(){
        var HTMLtraining =  '<tr><td>%training%</td><td>%order%</td>' + 
                            '<td><a id="edit-training" href="edit-training.html?t=%training_key%" class="btn-lg"><i class="fa fa-pencil" aria-hidden="true"></i></a>' + 
                            '<a id="delete-training" href="#" onclick="confirmDelete(\'%training_key%\');" class="btn-lg"><i class="fa fa-trash-o" aria-hidden="true"></i></a>',
            ERROR_MESSAGE = 'Missing order',
            tmpStr;

        //saved training message
        if(getParameterByName("s") == "success"){
            $("#bootstrap-override").prepend('<p class="alert alert-success">Training saved successfully</p>');
            $("#bootstrap-override .alert-success").fadeOut(3000, function(){
                this.remove();
            });
        }

        for(var i=0; i<trainingsArray.length; i++){
            for(key in trainingsArray[i]){
                tmpStr = HTMLtraining.replace(/%training_key%/g, key);
                tmpStr = tmpStr.replace('%training%', trainingsArray[i][key].en.title);
                if(trainingsArray[i][key].order){
                    tmpStr = tmpStr.replace('%order%', trainingsArray[i][key].order);
                    $('#table>tbody').prepend(tmpStr);
                }else{
                    tmpStr = tmpStr.replace('%order%', ERROR_MESSAGE);
                    $('#table>tbody').append(tmpStr);//print error trainings last
                }
            }
        }
    },

    /* 
        Delete full training from trainings.json.
        Update trainings.json.
        @param nTraining, key of training to delete
    */
    deleteTraining: function(nTraining){
        var self = this;
        
        for(var i=0; i<trainingsArray.length; i++){
            for(key in trainingsArray[i]){
                if(key.indexOf(nTraining) !== -1){

                    trainingsArray.splice(i, 1);

                    $('#table>tbody').empty();
                    self.printTrainings();

                    writeJSON(arrayToObject(trainingsArray));

                    return;
                }
            }
        }
        console.log("ERROR: could not find training: " + nTraining);
    }

};

$(document).ready(function() {
    $.when(getJSON())
        .then(function(){
            trainingsTable.initialize();
        })
});




