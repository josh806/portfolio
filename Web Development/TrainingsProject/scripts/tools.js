/*
    tools.js
    General functions shared between trainings.html and edit-training.html.
*/

var trainingsArray = [],//training objects
    jsonFile = "trainings.json";


function getJSON(){
    var defer = $.Deferred();

    $.getJSON(jsonFile, function(result) {
        $.each(result, function(type, trainings) {
            var obj = {};
            obj[type] = trainings;
            trainingsArray.push(obj);
        });

        if(trainingsArray.length === 0){
            defer.reject();
            return;
        }

        defer.resolve();
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("There was a problem locating " + jsonFile);
        console.log("error " + textStatus);
        console.log("incoming Text " + jqXHR.responseText);
    });

    return defer.promise();
}

/*
    Send JSON as string to server.
    @param json object.
*/
function writeJSON(json){
    $.ajax({
        type: "POST",
        dataType : 'json',
        url: 'scripts/save-json.php',
        data: {data: JSON.stringify(json)},
        success: function(response, textStatus, jqXHR){
                  console.log("writeJSON succeeded");
                },
        error: function(jqXHR, textStatus, errorThrown){
                  console.log("writeJSON failed");
                  console.log(errorThrown);
                }
    });
}

/*
    Turn an array of objects into an object.
    @param array to convert
    @return new object
*/
function arrayToObject(array){
    var result = null;

    if(array === null){
        console.log("ERROR: cannot convert array of null into an object");
    }else{
        result = {};
        for(var i=0; i<array.length; i++){
            for(key in array[i]){
                result[key] = array[i][key];
            }
        }
    }

    return result;
}

/*
    Confirm alert box to delete.
    @param id, identifier of object to delete
    @param arrayId, optional param for editTraining.html
*/
function confirmDelete(id, arrayId){
    var url = window.location.pathname;

    if(confirm("Are you sure you want to delete this training?") === true){
        if(url.indexOf("trainings.html") !== -1){
            trainingsTable.deleteTraining(id);
        }else if(url.indexOf("edit-training.html") !== -1){
            console.log("delete");
            editTraining.deleteTraining(id, arrayId);
        }
    }
}

/*
    Get variable from URL.
    @param name, of variable
    @param url
*/
function getParameterByName(name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/*
    If input field is empty, print error message
    @param field identifier
    @return true/false
*/
function isEmptyField(id){
    var currField = $(id),
        errClass = "invalid",
        emptyError = "Required field";

    currField.removeClass(errClass);

    if( !$(currField).val() ) {
        $(currField).addClass(errClass);
        currField.attr('placeholder',emptyError);
        return true
    }else return false;
}

/*
    Checks field contacins at least one letter
    @param field identifier
    @return true/false
*/
function containsLetters(id){
    var currField = $(id),
        string = currField.val(),
        errClass = "invalid";

    string = string.toLowerCase();
    string = string.replace(/[^a-z]/gi, '');

    if(string.length > 0){
        return true;
    }

    //print error message
    $(currField).addClass(errClass);
    currField.parent().append('<p class="invalid-text">Field must contain at least one letter</p>');
    return false;
}
