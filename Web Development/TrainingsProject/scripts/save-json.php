<?php

/*
	save-json.php
	Overwrite trainings.json file with new trainings object.	
*/

$fileName = "../trainings.json";
$file = fopen($fileName, 'w') or die("can't open file");
$stringData = $_POST["data"];
fwrite($file, $stringData);
fclose($file);

if($stringData == ''){
	exit(json_encode("json code is blank"));
}else exit(json_encode($stringData));

?>