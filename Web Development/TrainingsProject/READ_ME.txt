
Webinar Trainings
(Formaciónes de Webinar)

READ ME


ESP:

Aplicación de web para editar webinars.
Los webinars están guardados en trainings.json.

Páginas:
- index.html - página de inicio de sesión, no funciona. Pulsa el bóton de Login para saltar esta página.
- trainings.html - lista de formaciones de trainings.json
- edit-trainings.html - página para editar formaciones y crear nuevas.

-------------------------------------------------------

ENG:

Web app used for editing webinars.
Webinars saved in trainings.json.

Pages:
- index.html - login page, not functional. Press Login button to skip page.
- trainings.html - list of trainings from trainings.json
- edit-trainings.html - page for editing trainings and creating new ones.



