##############

# README #

##############

ENG:

Welcome to the technical portfolio of Josh McCarthy!

I welcome constructive criticism, so please don't hesitate to contact me if you believe that my code can be improved in any way.
I'm always motivated to learn!

Programs are organised by their language.

Each program contains a READ_ME.txt file explaining the goal of the program.

Please contact me with any questions or advice that you may have.

Thanks!

Josh McCarthy
joshcaterham@gmail.com
+34 684 24 64 24

######################################################

ESP:

Bienvenidos a el portfolio técnico de Josh McCarthy!

Estaré muy contento si me puedes dar críticas constructivas, así que, por favor, no dudes en contactarme si me puedes ayudar con la calidad de mi código.
Siempre estoy motivado para aprender!

Los programas son organizados por su idioma.

Cada programa tiene un archivo de READ_ME.txt explicando el objetivo del programa.

Por favor, contactarme con cualquier pregunta o consejo.

Gracias!

Josh McCarthy
joshcaterham@gmail.com
+34 684 24 64 24
