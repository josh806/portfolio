/*
	CubeDecimals Object.
	Used to create 3D cubes.
	@author Josh McCarthy
*/


import java.math.BigDecimal;

public class CubeDecimals {
	private BigDecimal x;
	private BigDecimal y;
	private BigDecimal z;
	private BigDecimal size;

	/*	
		Constructor.
		@param x,y,z coordinates of cube
		@param size of cube
	*/
	public CubeDecimals(BigDecimal x, BigDecimal y, BigDecimal z, BigDecimal size) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.size = size;
	}

	/* Accessors */
	public BigDecimal getX(){
		return this.x;
	}
	public BigDecimal getY(){
		return this.y;
	}
	public BigDecimal getZ(){
		return this.z;
	}
	public BigDecimal getSize(){
		return this.size;
	}

	/*
		Returns the corners of x, y or z values based on axis.
		@param axis, (x, y or z)
		@return array with two coordinates of cube on axis
	*/
	public BigDecimal[] getXyzCorners(char axis){
		BigDecimal[] vals = new BigDecimal[2];
		BigDecimal halfWidth = this.size.divide(new BigDecimal(2));

		switch(axis){
			case 'x':
				vals[0] = this.x.subtract(halfWidth);
				vals[1] = this.x.add(halfWidth);
				break;
			case 'y':
				vals[0] = this.y.subtract(halfWidth);
				vals[1] = this.y.add(halfWidth);
				break;
			case 'z':
				vals[0] = this.z.subtract(halfWidth);
				vals[1] = this.z.add(halfWidth);
				break;
			default:
				break;
		}

		return vals;
	}

}