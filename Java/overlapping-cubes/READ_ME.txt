
Overlapping Cubes
(intersección de cubos)

READ ME


ESP:

El usuario pone los coordinados del punto centro de dos cubos en un espacio 3D y el tamaño del cubo.
Los dos cubos son paralelos.
La aplicación calcula si los dos cubos se intersecan, y en el caso que si, devuelve el volumen del espacio compartido.

Por ejemplo:

Entrada:
Cube A:	Size = 5m	x = 10m		y = 10m		z = 0
Cube B: Size = 2m	x = 9m		y = 9m		z = 0

Salida:
"Overlap area: 8 metres cubed"

-------------------------------------------------------

ENG:

User enters the coordinates of the center point of two cubes in 3D space and the cubes' size.
The two cubes are parallel to each other.
The application calculates if the two cubes intersect, and if so, returns the volume of the shared space.

For example:

Input:
Cube A:	Size = 5m	x = 10m		y = 10m		z = 0
Cube B: Size = 2m	x = 9m		y = 9m		z = 0

Output:
"Overlap area: 8 metres cubed"


