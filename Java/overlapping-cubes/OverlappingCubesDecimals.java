/*
	Assumptions:
		- Cubes are parallel to one another

	@author Josh McCarthy
*/

import java.util.Scanner;
import java.math.BigDecimal;

public class OverlappingCubesDecimals {
	public static CubeDecimals cube1;
	public static CubeDecimals cube2;

	public static BigDecimal cube1Size;
	public static BigDecimal cube2Size;
	public static BigDecimal c1xVals[] = new BigDecimal[2];
	public static BigDecimal c1yVals[] = new BigDecimal[2];
	public static BigDecimal c1zVals[] = new BigDecimal[2];
	public static BigDecimal c2xVals[] = new BigDecimal[2];
	public static BigDecimal c2yVals[] = new BigDecimal[2];
	public static BigDecimal c2zVals[] = new BigDecimal[2];

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		BigDecimal cube1X, cube1Y, cube1Z;
		BigDecimal cube2X, cube2Y, cube2Z;


		System.out.println("\n-------------------------\nWelcome to Overlapping Cubes!\n-------------------------\n");


		/* Get and check user input */
		System.out.println("\n----CUBE 1----\n");
		cube1X = getBigDecimalFromScanner(scan, "Cube1 x coordinate:");
		cube1Y = getBigDecimalFromScanner(scan, "Cube1 y coordinate:");
		cube1Z = getBigDecimalFromScanner(scan, "Cube1 z coordinate:");
		while(true){//size can't be < 0 (doesn't catch -0)
			cube1Size = getBigDecimalFromScanner(scan, "Cube1 size (meters):");
			if(cube1Size.compareTo(BigDecimal.ZERO) < 0){
				System.out.println("Size can't be negative\n");
			}else break;
		}

		System.out.println("\n----CUBE 2----\n");
		cube2X = getBigDecimalFromScanner(scan, "Cube2 x coordinate:");
		cube2Y = getBigDecimalFromScanner(scan, "Cube2 y coordinate:");
		cube2Z = getBigDecimalFromScanner(scan, "Cube2 z coordinate:");
		while(true){//size can't be < 0 (doesn't catch -0)
			cube2Size = getBigDecimalFromScanner(scan, "Cube2 size (meters):");
			if(cube2Size.compareTo(BigDecimal.ZERO) < 0){
				System.out.println("Size can't be negative\n");
			}else break;
		}


		/* Create Cubes */
		cube1 = new CubeDecimals(cube1X, cube1Y, cube1Z, cube1Size);
		cube2 = new CubeDecimals(cube2X, cube2Y, cube2Z, cube2Size);


		/* Get x, y, and z values of cubes */
		c1xVals = cube1.getXyzCorners('x');
		c1yVals = cube1.getXyzCorners('y');
		c1zVals = cube1.getXyzCorners('z');

		c2xVals = cube2.getXyzCorners('x');
		c2yVals = cube2.getXyzCorners('y');
		c2zVals = cube2.getXyzCorners('z');


		/* Print result */
		System.out.println(isOverlap());
	}

	/*
		@param Scanner
		@param instruction for user
		@return BigDecimal from scanner
	*/
	public static BigDecimal getBigDecimalFromScanner(Scanner scan, String instruction){

		while(true){
			System.out.println(instruction);
			if(scan.hasNextBigDecimal()){
				break;
			}else{
				System.out.println("ERROR: Not valid BigDecimal value\n");
				scan.next();
			}
		}
		return scan.nextBigDecimal();
	}

	/*
		Determine if there is an overlap and return the relevant string.
		@return string result of overlap (no overlap || overlap volume)
	*/
	public static String isOverlap(){

		if((c1xVals[1].compareTo(c2xVals[0])) > 0 && (c1xVals[0].compareTo(c2xVals[1])) < 0 && (c1yVals[1].compareTo(c2yVals[0])) > 0 && (c1yVals[0].compareTo(c1yVals[1])) < 0 && (c1zVals[1].compareTo(c2zVals[0])) > 0 && (c1zVals[0].compareTo(c2zVals[1])) < 0){

			System.out.println("\nCubes overlap!");

			BigDecimal xOverlap = calcOverlap(c1xVals, c2xVals);
			if(xOverlap.compareTo(new BigDecimal(-1)) == 0) xOverlap = cube1Size;//xOverlap is full cube size

			BigDecimal yOverlap = calcOverlap(c1yVals, c2yVals);
			if(yOverlap.compareTo(new BigDecimal(-1)) == 0) yOverlap = cube1Size;//yOverlap is full cube size

			BigDecimal zOverlap = calcOverlap(c1zVals, c2zVals);
			if(zOverlap.compareTo(new BigDecimal(-1)) == 0) zOverlap = cube1Size;//zOverlap is full cube size


			System.out.println("x:" + xOverlap + "|y:" + yOverlap + "|z:" + zOverlap);

			BigDecimal overlapVolume = (xOverlap.multiply(yOverlap)).multiply(zOverlap);
			return "\nOverlap area: " + overlapVolume + " metres cubed";
		}else{
			return "\nCubes do not overlap";
		}
	}

	/*
		Calculate overlap of two cubes on on particular axis e.g. x
		@param c1_Vals & c2_Vals, array[2] e.g. [0] value x1, [1] value x2.
		@return overlap of axes if overlap, else return -1 if axes the same i.e. overlap is size of cube.
	*/
	public static BigDecimal calcOverlap(BigDecimal[] c1_Vals, BigDecimal[] c2_Vals){
		BigDecimal overlap = new BigDecimal(-1);

		// All of c1 inside c2
		// c2 left < c1 left && c1 right < c2 right
		if((c2_Vals[0].compareTo(c1_Vals[0])) < 0 && (c1_Vals[1].compareTo(c2_Vals[1])) < 0){
			overlap = cube1.getSize();
			System.out.println("All of c1 inside c2");
		}else

		// All of c2 inside c1
		// c1 left < c2 left && c2 right < c1 right
		if((c1_Vals[0].compareTo(c2_Vals[0])) < 0 && (c2_Vals[1].compareTo(c1_Vals[1])) < 0){
			overlap = cube2.getSize();
			System.out.println("All of c2 inside c1");
		}else

		// c1-1 is inside c2
		// c2 left < c1 left && c2 right > c1 left
		if((c2_Vals[0].compareTo(c1_Vals[0])) < 0 && (c2_Vals[1].compareTo(c1_Vals[0])) > 0){
			overlap = c2_Vals[1].subtract(c1_Vals[0]);
			System.out.println("c1-1 is inside c2");
		}else

		// c1-2 is inside c2
		// c2 left < c1 right && c2 right > c1 right
		if((c2_Vals[0].compareTo(c1_Vals[1])) < 0 && (c2_Vals[1].compareTo(c1_Vals[1])) > 0){
			overlap = c1_Vals[1].subtract(c2_Vals[0]);
			System.out.println("c1-2 is inside c2");
		}

		return overlap;
	}

}
